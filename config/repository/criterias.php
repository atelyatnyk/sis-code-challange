<?php
return [
    'schema' => [
        'default' => [
            \App\Utilites\Repositories\Criterias\Contracts\DefaultCriteriaDictionary::BY_KEY =>
                \App\Utilites\Repositories\Criterias\DefaultCriterias\ByKey::class,

            \App\Utilites\Repositories\Criterias\Contracts\DefaultCriteriaDictionary::WITH_RELATION =>
                \App\Utilites\Repositories\Criterias\DefaultCriterias\WithRelation::class,

            \App\Utilites\Repositories\Criterias\Contracts\DefaultCriteriaDictionary::BY_RELATION =>
                \App\Utilites\Repositories\Criterias\DefaultCriterias\ByRelation::class,

            \App\Utilites\Repositories\Criterias\Contracts\DefaultCriteriaDictionary::WITH_DELETED =>
                \App\Utilites\Repositories\Criterias\DefaultCriterias\WithDeleted::class,
        ],
        'expenses' => [
            \App\Src\Expenses\Repository\Contracts\ExpensesCriteriaDictionary::BY_EMPLOYEE_ID =>
                \App\Src\Expenses\Repository\Criterias\ByEmpoyeeId::class,
            \App\Src\Expenses\Repository\Contracts\ExpensesCriteriaDictionary::DATE_BETWEEN =>
                \App\Src\Expenses\Repository\Criterias\DateBetween::class,
            \App\Src\Expenses\Repository\Contracts\ExpensesCriteriaDictionary::SORT_CATEGORY =>
                \App\Src\Expenses\Repository\Criterias\SortByCategory::class,
            \App\Src\Expenses\Repository\Contracts\ExpensesCriteriaDictionary::ORDER_BY_DATE =>
                \App\Src\Expenses\Repository\Criterias\OrderByDate::class,
        ],
        'employee' => [
            \App\Src\Employee\Repository\Contracts\EmployeeCriteriaDictionary::BY_USER_ID =>
                \App\Src\Employee\Repository\Criterias\ByUserId::class,
            \App\Src\Employee\Repository\Contracts\EmployeeCriteriaDictionary::BY_NAME =>
                \App\Src\Employee\Repository\Criterias\ByName::class,
            \App\Src\Employee\Repository\Contracts\EmployeeCriteriaDictionary::BY_ADDRESS =>
                \App\Src\Employee\Repository\Criterias\ByAddress::class,
        ],
        'user' => [
            \App\Src\User\Repository\Contracts\UserCriteriaDictionary::BY_NOT_ASSIGNED =>
                \App\Src\Employee\Repository\Criterias\ByNotAssigned::class,
        ],
    ]
];
