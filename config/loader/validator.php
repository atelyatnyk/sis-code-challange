<?php

return [
    'expenses' => [
        'date',
        'category',
        'employee_name',
        'employee_address',
        'expense_description',
        'pre_tax_amount',
        'tax_amount',
    ]
];