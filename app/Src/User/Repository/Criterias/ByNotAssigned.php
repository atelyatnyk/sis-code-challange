<?php

namespace App\Src\Employee\Repository\Criterias;

use App\Utilites\Repositories\Criterias\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

class ByNotAssigned implements Criteria
{
    private $value;

    /**
     * ByUserId constructor.
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $query
     * @return Builder|mixed
     */
    public function apply(Builder $query)
    {
        return $query->whereDoesntHave('employees');
    }

}
