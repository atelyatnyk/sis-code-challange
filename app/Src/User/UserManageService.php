<?php

namespace App\Src\User;

use App\Src\User\Contracts\UserManageService as UserManageServiceInterface;
use App\Src\User\Repository\Contracts\UserRepository;
use Illuminate\Database\Eloquent\Collection;

class UserManageService implements UserManageServiceInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserManageService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return Collection|mixed
     */
    public function getNotAssigned()
    {
        return $this->userRepository->findBy([
                $this->userRepository::BY_NOT_ASSIGNED => true,
            ]);
    }

}
