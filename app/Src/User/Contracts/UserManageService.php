<?php

namespace App\Src\User\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface UserManageService
{

    /**
     * @return Collection|mixed
     */
    public function getNotAssigned();

}
