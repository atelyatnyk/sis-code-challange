<?php

namespace App\Src\Employee;

use App\Models\Employee;
use App\Src\Employee\Contracts\EmployeeManageService as EmployeeManageServiceInterface;
use App\Src\Employee\Repository\Contracts\EmployeeRepository;
use Illuminate\Database\Eloquent\Collection;

class EmployeeManageService implements EmployeeManageServiceInterface
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepository;

    /**
     * EmployeeManageService constructor.
     * @param EmployeeRepository $employeeRepository
     */
    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * @param array $data
     * @return Employee
     */
    public function findOrCreate(array $data): Employee
    {
        $name = $data[Employee::COLUMN_NAME];
        $address = $data[Employee::COLUMN_ADDRESS];

        $employees = $this->employeeRepository->findBy([
            $this->employeeRepository::BY_NAME => $name,
            $this->employeeRepository::BY_ADDRESS => $address
        ]);

        $employee = $employees->isNotEmpty()
            ? $employees->first()
            : $this->employeeRepository->create([
                Employee::COLUMN_NAME => $name,
                Employee::COLUMN_ADDRESS => $address
            ]);

        return $employee;
    }

    /**
     * @param int $perPage
     * @return Collection|mixed
     */
    public function getNotAssignedPaginated(int $perPage)
    {
        return $this->employeeRepository->findPaginated([
            $this->employeeRepository::BY_USER_ID => null,
        ],
            $perPage);
    }

    /**
     * @param Employee $employee
     * @param int $userId
     * @return bool
     */
    public function assignUser(Employee $employee, int $userId): bool
    {
        try {
            $employee->setUserId($userId);
            $this->employeeRepository->save($employee);

            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }


}
