<?php

namespace App\Src\Employee\Contracts;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Collection;

interface EmployeeManageService
{
    /**
     * @param array $data
     * @return Employee
     */
    public function findOrCreate(array $data): Employee;

    /**
     * @param int $perPage
     * @return Collection|mixed
     */
    public function getNotAssignedPaginated(int $perPage);

    /**
     * @param Employee $employee
     * @param int $userId
     * @return bool
     */
    public function assignUser(Employee $employee, int $userId): bool;

}
