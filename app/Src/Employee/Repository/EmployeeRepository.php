<?php

namespace App\Src\Employee\Repository;

use App\Models\Employee;
use App\Src\Employee\Repository\Contracts\EmployeeRepository as EmployeeRepositoryInterface;
use App\Utilites\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;

class EmployeeRepository extends Repository implements EmployeeRepositoryInterface
{
    /**
     * EmployeeRepository constructor.
     * @param EmployeeCriteriaFactory $factory
     * @param Employee $model
     * @throws \App\Utilites\Repositories\Exceptions\RepositoryException
     */
    public function __construct(EmployeeCriteriaFactory $factory, Employee $model)
    {
        parent::__construct($factory, $model);
    }

    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Employee;
    }

}
