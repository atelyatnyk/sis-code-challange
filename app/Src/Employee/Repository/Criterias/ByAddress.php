<?php

namespace App\Src\Employee\Repository\Criterias;

use App\Models\Employee;
use App\Utilites\Repositories\Criterias\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

class ByAddress implements Criteria
{
    private $value;

    /**
     * ByAddress constructor.
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $query
     * @return Builder|mixed
     */
    public function apply(Builder $query)
    {
        return $query->where(Employee::COLUMN_ADDRESS, $this->value);
    }
}
