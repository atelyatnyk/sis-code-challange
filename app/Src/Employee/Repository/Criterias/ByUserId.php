<?php

namespace App\Src\Employee\Repository\Criterias;

use App\Models\Employee;
use App\Utilites\Repositories\Criterias\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

class ByUserId implements Criteria
{
    private $value;

    /**
     * ByUserId constructor.
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $query
     * @return Builder|mixed
     */
    public function apply(Builder $query)
    {
        return $query->where(Employee::COLUMN_USER_ID, $this->value);
    }
}