<?php

namespace App\Src\Employee\Repository;

use App\Utilites\Repositories\Criterias\CriteriaFactory;

class EmployeeCriteriaFactory extends CriteriaFactory
{
    const CONTEXT = 'employee';

    /**
     * @return string
     */
    protected function getContext(): string
    {
        return self::CONTEXT;
    }

}