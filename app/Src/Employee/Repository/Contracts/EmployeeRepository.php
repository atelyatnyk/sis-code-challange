<?php

namespace App\Src\Employee\Repository\Contracts;

use App\Utilites\Repositories\Contracts\Repository;

interface EmployeeRepository extends Repository, EmployeeCriteriaDictionary
{

}