<?php

namespace App\Src\Employee\Repository\Contracts;

use App\Models\Employee;
use App\Utilites\Repositories\Criterias\Contracts\DefaultCriteriaDictionary;

interface EmployeeCriteriaDictionary extends DefaultCriteriaDictionary
{
    public const BY_USER_ID = Employee::COLUMN_USER_ID;
    public const BY_NAME = Employee::COLUMN_NAME;
    public const BY_ADDRESS = Employee::COLUMN_ADDRESS;
}