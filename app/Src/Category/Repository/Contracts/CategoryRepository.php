<?php

namespace App\Src\Category\Repository\Contracts;

use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;

interface CategoryRepository
{
    /**
     * @param array $data
     * @return Category
     */
    public function create(array $data): Category;

    /**
     * @param string $slug
     * @return Category|null
     */
    public function getBySlug(string $slug): ?Category;

    /**
     * @return Collection
     */
    public function getAll(): Collection;
}