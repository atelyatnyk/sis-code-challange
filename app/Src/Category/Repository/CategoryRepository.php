<?php

namespace App\Src\Category\Repository;

use App\Models\Category;
use App\Src\Category\Repository\Contracts\CategoryRepository as CategoryRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository implements CategoryRepositoryInterface
{
    /**
     * @var Category
     */
    private $category;

    /**
     * CategoryRepository constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @param array $data
     * @return Category
     */
    public function create(array $data): Category
    {
        /**
         * @var Category $category
         */
        $category = $this->category->newQuery()->create($data);

        return $category;
    }

    /**
     * @param string $slug
     * @return Category|null
     */
    public function getBySlug(string $slug): ?Category
    {
        /**
         * @var Collection $categories
         */
        $categories = $this->category->newQuery()->where(Category::COLUMN_SLUG, $slug)->get();

        return $categories->isEmpty()
            ? null
            : $categories->first();
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->category->newQuery()->get();
    }

}