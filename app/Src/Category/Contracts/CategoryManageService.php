<?php

namespace App\Src\Category\Contracts;

use App\Models\Category;

interface CategoryManageService
{
    /**
     * @param string $category
     * @return mixed
     */
    public function findOrCreate(string $category): Category;
}