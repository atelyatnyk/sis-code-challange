<?php

namespace App\Src\Category;

use App\Models\Category;
use App\Src\Category\Contracts\CategoryManageService as CategoryManageServiceInterface;
use App\Src\Category\Repository\Contracts\CategoryRepository;
use Illuminate\Database\Eloquent\Collection;

class CategoryManageService implements CategoryManageServiceInterface
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * CategoryManageService constructor.
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param string $category
     * @return Category|mixed
     */
    public function findOrCreate(string $category): Category
    {
        /**
         * @var Collection $categories
         */
        $categories = $this->categoryRepository->getBySlug(str_slug($category));

        return $categories
            ? $categories
            : $this->categoryRepository->create(
                [
                    Category::COLUMN_TITLE => $category,
                    Category::COLUMN_SLUG => str_slug($category),
                ]
            );
    }

}