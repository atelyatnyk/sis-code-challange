<?php

namespace App\Src\Expenses\Contracts;

use Illuminate\Http\UploadedFile;

interface ImportService
{
    /**
     * @param UploadedFile $file
     * @return mixed
     */
    public function import(UploadedFile $file);
}