<?php

namespace App\Src\Expenses\Contracts;

use App\Models\Expense;
use App\Models\User;

interface ExpensesManageService
{
    /**
     * @param \App\DTO\Expense $data
     * @return Expense
     */
    public function create(\App\DTO\Expense $data): Expense;

    /**
     * @param User $user
     * @param int $perPage
     * @param array $filters
     * @return mixed
     */
    public function getAllPaginated(User $user, int $perPage, array $filters);

    /**
     * @param array $data
     * @return Expense
     */
    public function import(array $data): Expense;
}