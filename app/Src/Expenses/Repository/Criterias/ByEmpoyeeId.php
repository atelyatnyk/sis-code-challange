<?php

namespace App\Src\Expenses\Repository\Criterias;

use App\Models\Expense;
use App\Utilites\Repositories\Criterias\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

class ByEmpoyeeId implements Criteria
{
    private $value;

    /**
     * ByEmpoyeeId constructor.
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $query
     * @return Builder|mixed
     */
    public function apply(Builder $query)
    {
        return $query->where(Expense::COLUMN_EMPLOYEE_ID, $this->value);
    }

}