<?php

namespace App\Src\Expenses\Repository\Criterias;

use App\Models\Expense;
use App\Utilites\Repositories\Criterias\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

class DateBetween implements Criteria
{
    private $value;

    /**
     * DateBetween constructor.
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $query
     * @return Builder|\Illuminate\Database\Query\Builder|mixed
     */
    public function apply(Builder $query)
    {
        $from = data_get($this->value, 'from');
        $to = data_get($this->value, 'to');

        return $query->whereDate(Expense::COLUMN_DATE, '>=', $from)
            ->whereDate(Expense::COLUMN_DATE, '<=', $to);
    }


}