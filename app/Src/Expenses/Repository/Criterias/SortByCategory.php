<?php

namespace App\Src\Expenses\Repository\Criterias;

use App\Utilites\Repositories\Criterias\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SortByCategory implements Criteria
{
    private $value;

    /**
     * SortByCategory constructor.
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $query
     * @return Builder|mixed
     */
    public function apply(Builder $query)
    {
        return $this->value
            ? $query->join('categories', 'expenses.category_id', '=', 'categories.id')
                ->orderBy('categories.title')
                ->select('*')
            : $query->join('categories', 'expenses.category_id', '=', 'categories.id')
                ->orderByDesc('categories.title')
                ->select('*');

    }


}