<?php

namespace App\Src\Expenses\Repository\Criterias;

use App\Models\Expense;
use App\Utilites\Repositories\Criterias\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

class OrderByDate implements Criteria
{
    private $value;

    /**
     * OrderByDate constructor.
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $query
     * @return Builder|mixed
     */
    public function apply(Builder $query)
    {
        return $this->value
            ? $query->orderBy(Expense::COLUMN_DATE)
            : $query->orderByDesc(Expense::COLUMN_DATE);
    }

}