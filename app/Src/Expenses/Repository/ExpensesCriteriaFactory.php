<?php

namespace App\Src\Expenses\Repository;

use App\Utilites\Repositories\Criterias\CriteriaFactory;

class ExpensesCriteriaFactory extends CriteriaFactory
{
    const CONTEXT = 'expenses';

    /**
     * @return string
     */
    protected function getContext(): string
    {
        return self::CONTEXT;
    }
}
