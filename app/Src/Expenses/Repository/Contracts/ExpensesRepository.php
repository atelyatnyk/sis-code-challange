<?php

namespace App\Src\Expenses\Repository\Contracts;

use App\Utilites\Repositories\Contracts\Repository;

interface ExpensesRepository extends Repository, ExpensesCriteriaDictionary
{

}