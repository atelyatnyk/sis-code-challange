<?php

namespace App\Src\Expenses\Repository\Contracts;

use App\Models\Expense;
use App\Utilites\Repositories\Criterias\Contracts\DefaultCriteriaDictionary;

interface ExpensesCriteriaDictionary extends DefaultCriteriaDictionary
{
    public const BY_EMPLOYEE_ID = Expense::COLUMN_EMPLOYEE_ID;
    public const DATE_BETWEEN = 'date_between';
    public const SORT_CATEGORY = 'sort_category';
    public const ORDER_BY_DATE= 'order_by_date';
}