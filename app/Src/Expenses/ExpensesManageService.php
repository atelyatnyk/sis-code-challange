<?php

namespace App\Src\Expenses;

use App\Models\Expense;
use App\Models\Role;
use App\Models\User;
use App\Src\Category\Contracts\CategoryManageService;
use App\Src\Expenses\Contracts\ExpensesManageService as ExpensesManageServiceAliasInterface;
use App\Src\Expenses\Repository\Contracts\ExpensesRepository;

class ExpensesManageService implements ExpensesManageServiceAliasInterface
{
    /**
     * @var ExpensesRepository
     */
    private $expensesRepository;

    /**
     * @var CategoryManageService
     */
    private $categoryManageService;

    /**
     * ExpensesManageService constructor.
     * @param ExpensesRepository $expensesRepository
     * @param CategoryManageService $categoryManageService
     */
    public function __construct(ExpensesRepository $expensesRepository, CategoryManageService $categoryManageService)
    {
        $this->expensesRepository = $expensesRepository;
        $this->categoryManageService = $categoryManageService;
    }


    /**
     * @param \App\DTO\Expense $data
     * @return Expense
     */
    public function create(\App\DTO\Expense $data): Expense
    {
        $category = $this->categoryManageService->findOrCreate($data->getCategory());
        $data->setCategoryId($category->getKey());
        /**
         * @var Expense $expense
         */
        $expense = $this->expensesRepository->create($data->toExpenseData());

        return $expense;
    }

    /**
     * @param User $user
     * @param int $perPage
     * @param array $filters
     * @return mixed
     */
    public function getAllPaginated(User $user, int $perPage, array $filters)
    {
        return $user->hasRole(Role::ROLE_ADMIN)
            ? $this->expensesRepository->findPaginated($filters, $perPage)
            : $this->expensesRepository->findPaginated(
                array_merge($filters, [$this->expensesRepository::BY_EMPLOYEE_ID => $user->getEmployee()->getKey()]),
                $perPage);

    }

    /**
     * @param array $data
     * @return Expense
     */
    public function import(array $data): Expense
    {
        /**
         * @var Expense $expense
         */
        $expense = $this->expensesRepository->create($data);

        return $expense;
    }


}