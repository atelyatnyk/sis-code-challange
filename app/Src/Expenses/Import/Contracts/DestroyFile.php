<?php

namespace App\Src\Expenses\Import\Contracts;

interface DestroyFile
{
    /**
     * @param string $pathToFile
     * @return mixed
     */
    public function destroy(string $pathToFile);
}