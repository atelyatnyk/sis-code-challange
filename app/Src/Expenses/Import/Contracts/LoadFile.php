<?php

namespace App\Src\Expenses\Import\Contracts;

use Illuminate\Http\UploadedFile;

interface LoadFile
{
    /**
     * @param UploadedFile $file
     * @return string
     */
    public function load(UploadedFile $file): string;
}