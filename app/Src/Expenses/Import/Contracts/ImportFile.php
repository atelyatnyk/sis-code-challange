<?php

namespace App\Src\Expenses\Import\Contracts;

interface ImportFile
{
    /**
     * @param string $pathToFile
     * @return mixed
     */
    public function import(string $pathToFile);
}