<?php

namespace App\Src\Expenses\Import;

use App\Models\Expense;
use Illuminate\Support\Collection;

class Calculator
{
    const PRE_TAX_AMOUNT = 'pre_tax_amount';
    const TAX_AMOUNT = 'tax_amount';
    const TOTAL = 'total';

    /**
     * @param Collection $imported
     * @return Collection
     */
    public function calculateMonthsExpenses(Collection $imported)
    {
        $result = new Collection();

        $imported->each(function (Expense $expense) use ($result) {
            $month = $expense->getDate()->monthName;

            $result = $this->calculateMonthExpenses($expense, $result, $month);
        });

        $result = $this->putTotal($imported, $result);

        return $result;
    }

    /**
     * @param Expense $expense
     * @param Collection $result
     * @param string $month
     * @return Collection
     */
    private function calculateMonthExpenses(Expense $expense, Collection $result, string $month): Collection
    {
        $monthSum = $result->get($month) ?: new Collection();
        $tax = $monthSum->get(self::TAX_AMOUNT) ?: 0;
        $preTax = $monthSum->get(self::PRE_TAX_AMOUNT) ?: 0;

        $tax += $expense->getTaxAmount();
        $preTax += $expense->getPreTaxAmount();

        $monthSum->put(self::TAX_AMOUNT, $tax);
        $monthSum->put(self::PRE_TAX_AMOUNT, $preTax);

        $result->put($month, $monthSum);

        return $result;
    }

    /**
     * @param Collection $imported
     * @return float
     */
    private function calculateTotalTax(Collection $imported): float
    {
        return $imported->sum(function (Expense $expense) {
            return $expense->getTaxAmount();
        });

    }

    /**
     * @param Collection $imported
     * @return float
     */
    private function calculateTotalPreTax(Collection $imported): float
    {
        return $imported->sum(function (Expense $expense) {
            return $expense->getPreTaxAmount();
        });

    }

    /**
     * @param Collection $imported
     * @param Collection $result
     * @return Collection
     */
    protected function putTotal(Collection $imported, Collection $result): Collection
    {
        $result->put(self::TOTAL, new Collection([
            self::TAX_AMOUNT => $this->calculateTotalTax($imported),
            self::PRE_TAX_AMOUNT => $this->calculateTotalPreTax($imported),
        ]));

        return $result;
    }
}