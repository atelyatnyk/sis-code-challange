<?php

namespace App\Src\Expenses\Import;

use App\Src\Expenses\Contracts\ExpensesManageService;
use App\Src\Expenses\Import\Contracts\ImportFile as ImportFileInterface;
use App\Utilites\Adapters\FastExcel\FastExcel;

class ImportFile implements ImportFileInterface
{
    /**
     * @var FastExcel
     */
    private $export;

    /**
     * @var ExpensesManageService
     */
    private $expensesManageService;

    /**
     * @var Converter
     */
    private $converter;

    /**
     * ImportFile constructor.
     * @param FastExcel $export
     * @param ExpensesManageService $expensesManageService
     * @param Converter $converter
     */
    public function __construct(FastExcel $export, ExpensesManageService $expensesManageService, Converter $converter)
    {
        $this->export = $export;
        $this->expensesManageService = $expensesManageService;
        $this->converter = $converter;
    }

    public function import(string $pathToFile)
    {
        return $this->export
            ->configureCsv('|', '"', '\n', 'UTF-8')
            ->import($pathToFile, function ($line) {
                $data = $this->convertToExpenseData($line);

                return $this->expensesManageService->import($data);
            });
    }

    /**
     * @param array $line
     * @return array
     */
    private function convertToExpenseData(array $line)
    {
        return $this->converter->toExpenseData($line);
    }
}