<?php

namespace App\Src\Expenses\Import;

use App\Models\Category;
use App\Models\Employee;
use App\Models\Expense;
use App\Src\Category\Contracts\CategoryManageService;
use App\Src\Employee\Contracts\EmployeeManageService;
use Carbon\Carbon;

class Converter
{
    public const HEADER_DATE = 'date';
    public const HEADER_CATEGORY = 'category';
    public const HEADER_EMPLOYEE_NAME = 'employee_name';
    public const HEADER_EMPLOYEE_ADDRESS = 'employee_address';
    public const HEADER_DESCRIPTION = 'expense_description';
    public const HEADER_PRE_TAX_AMOUNT = 'pre_tax_amount';
    public const HEADER_TAX_AMOUNT = 'tax_amount';

    /**
     * @var CategoryManageService
     */
    private $categoryManageService;

    /**
     * @var EmployeeManageService
     */
    private $employeeManageService;

    /**
     * Converter constructor.
     * @param CategoryManageService $categoryManageService
     * @param EmployeeManageService $employeeManageService
     */
    public function __construct(CategoryManageService $categoryManageService, EmployeeManageService $employeeManageService)
    {
        $this->categoryManageService = $categoryManageService;
        $this->employeeManageService = $employeeManageService;
    }

    /**
     * @param array $line
     * @return array
     */
    public function toExpenseData(array $line): array
    {
        $category = $line[self::HEADER_CATEGORY];
        $name = $line[self::HEADER_EMPLOYEE_NAME];
        $adress = $line[self::HEADER_EMPLOYEE_ADDRESS];

        $category = $this->getCategory($category);
        $employee = $this->getEmployee($name, $adress);

        return [
            Expense::COLUMN_DATE => Carbon::parse($line[self::HEADER_DATE]),
            Expense::COLUMN_CATEGORY_ID => $category->getKey(),
            Expense::COLUMN_EMPLOYEE_ID => $employee->getKey(),
            Expense::COLUMN_DESCRIPTION => $line[self::HEADER_DESCRIPTION],
            Expense::COLUMN_PRE_TAX_AMOUNT => $line[self::HEADER_PRE_TAX_AMOUNT],
            Expense::COLUMN_TAX_AMOUNT => $line[self::HEADER_TAX_AMOUNT],
        ];

    }

    /**
     * @param string $category
     * @return Category
     */
    private function getCategory(string $category): Category
    {
        return $this->categoryManageService->findOrCreate($category);
    }

    /**
     * @param string $name
     * @param string $address
     * @return Employee
     */
    private function getEmployee(string $name, string $address): Employee
    {
        return $this->employeeManageService->findOrCreate([
            Employee::COLUMN_NAME => $name,
            Employee::COLUMN_ADDRESS => $address]);
    }
}