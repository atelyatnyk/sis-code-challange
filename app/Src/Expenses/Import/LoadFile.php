<?php

namespace App\Src\Expenses\Import;

use App\Src\Expenses\Import\Contracts\LoadFile as LoadServiceInterface;
use Illuminate\Filesystem\FilesystemManager as Storage;
use Illuminate\Http\UploadedFile;

class LoadFile implements LoadServiceInterface
{
    const DIRECTORY = 'import';
    /**
     * @var Storage
     */
    private $storage;

    /**
     * LoadFile constructor.
     * @param Storage $storage
     */
    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    public function load(UploadedFile $file): string
    {
        $directoryPath = $this->createImportDirectory();

        $file = $this->storage->disk('local')->putFileAs(
            $directoryPath,
            $file,
            $file->getFilename() . '.' . $file->getClientOriginalExtension()
        );
        $file = $this->storage->disk('local')->getDriver()->getAdapter()->getPathPrefix() . $file;

        return $file;
    }

    /**
     * @return string
     */
    private function createImportDirectory(): string
    {
        $this->storage->disk('local')->makeDirectory(self::DIRECTORY);

        return self::DIRECTORY;
    }
}