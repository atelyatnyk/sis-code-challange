<?php

namespace App\Src\Expenses\Import;

use App\Src\Expenses\Import\Contracts\DestroyFile as DestroyFileInterface;
use Illuminate\Filesystem\FilesystemManager as Storage;

class DestroyFile implements DestroyFileInterface
{
    /**
     * @var Storage
     */
    private $storage;

    /**
     * DestroyFile constructor.
     * @param Storage $storage
     */
    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param string $pathToFile
     */
    public function destroy(string $pathToFile): void
    {
        $filePathPrefix = $this->storage->disk('local')->getDriver()->getAdapter()->getPathPrefix();
        $fileName = str_replace($filePathPrefix, '', $pathToFile);
        $this->storage->disk('local')->delete($fileName);
    }
}