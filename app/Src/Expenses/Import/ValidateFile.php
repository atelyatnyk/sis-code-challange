<?php

namespace App\Src\Expenses\Import;

use App\Utilites\Adapters\Config\Contracts\ConfigRepository;
use App\Utilites\Adapters\FastExcel\FastExcel;

class ValidateFile
{
    const EXPENSES_HEADERS = 'loader.validator.expenses';

    /**
     * @var FastExcel
     */
    private $fastExcel;

    /**
     * @var ConfigRepository
     */
    private $configRepository;

    /**
     * @var array
     */
    private $validations;

    /**
     * @var array
     */
    private $requiredColumns;

    /**
     * @param FastExcel $fastExcel
     * @param ConfigRepository $configRepository
     */
    public function __construct(FastExcel $fastExcel, ConfigRepository $configRepository)
    {
        $this->fastExcel = $fastExcel;
        $this->configRepository = $configRepository;
        $this->requiredColumns = $this->getValidations();
    }

    /**
     * @param string $source
     * @return bool
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
     */
    public function validate(string $source): bool
    {
        $isValid = false;

        $this->fastExcel->configureCsv('|', '"', '\n', 'UTF-8')
            ->import($source, function ($line) use (&$isValid) {
                $isValid = $this->validateHeaders($line);

                return;
            });

        return $isValid;

    }

    /**
     * @param $line
     * @return bool
     */
    private function validateHeaders($line): bool
    {
        $columns = array_keys($line);

        $isValid = count(array_intersect($this->requiredColumns, $columns)) === count($this->requiredColumns);

        return $isValid;
    }

    /**
     * @return array
     */
    private function getValidations()
    {
        return $this->configRepository->get(self::EXPENSES_HEADERS);
    }
}