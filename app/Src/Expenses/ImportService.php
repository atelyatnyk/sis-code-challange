<?php

namespace App\Src\Expenses;

use App\Src\Expenses\Contracts\ImportService as ImportServiceInterface;
use App\Src\Expenses\Exceptions\ImportException;
use App\Src\Expenses\Import\Calculator;
use App\Src\Expenses\Import\Contracts\DestroyFile;
use App\Src\Expenses\Import\Contracts\ImportFile;
use App\Src\Expenses\Import\Contracts\LoadFile;
use App\Src\Expenses\Import\ValidateFile;
use Illuminate\Http\UploadedFile;

class ImportService implements ImportServiceInterface
{
    /**
     * @var LoadFile
     */
    private $loader;

    /**
     * @var DestroyFile
     */
    private $destroyer;

    /**
     * @var ImportFile
     */
    private $importer;

    /**
     * @var ValidateFile
     */
    private $validator;

    /**
     * @var Calculator
     */
    private $calculator;

    /**
     * ImportService constructor.
     * @param LoadFile $loader
     * @param DestroyFile $destroyer
     * @param ImportFile $importer
     * @param ValidateFile $validator
     * @param Calculator $calculator
     */
    public function __construct(
        LoadFile $loader,
        DestroyFile $destroyer,
        ImportFile $importer,
        ValidateFile $validator,
        Calculator $calculator
    ) {
        $this->loader = $loader;
        $this->destroyer = $destroyer;
        $this->importer = $importer;
        $this->validator = $validator;
        $this->calculator = $calculator;
    }


    /**
     * @param UploadedFile $file
     * @return bool|mixed
     * @throws ImportException
     */
    public function import(UploadedFile $file)
    {
        try {
            $pathToFile = $this->loader->load($file);
            $isValid = $this->validator->validate($pathToFile);
            if (!$isValid) {
                $this->destroyer->destroy($pathToFile);

                return false;
            }
            $imported = $this->importer->import($pathToFile);
            $this->destroyer->destroy($pathToFile);

            return $this->calculator->calculateMonthsExpenses($imported);
        } catch (\Exception $exception) {
            throw  new ImportException("An error occurred during the import");
        } finally {
            $this->destroyer->destroy($pathToFile);
        }
    }

}