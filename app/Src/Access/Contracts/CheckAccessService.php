<?php

namespace App\Src\Access\Contracts;

use App\Models\User;

interface CheckAccessService
{
    public function checkPermissionGranted(User $user, string $route): bool;

}
