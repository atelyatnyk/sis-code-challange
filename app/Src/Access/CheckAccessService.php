<?php

namespace App\Src\Access;

use App\Models\User;
use App\Src\Access\Contracts\CheckAccessService as CheckAccessServiceInterface;
use App\Utilites\Adapters\Config\Contracts\ConfigRepository;

class CheckAccessService implements CheckAccessServiceInterface
{
    const SCHEMA = 'permissions.routesRoles.schema';

    /**
     * @var array
     */
    private $schema = [];

    /**
     * @var ConfigRepository
     */
    private $configRepository;

    /**
     * CheckAccessService constructor.
     * @param ConfigRepository $configRepository
     */
    public function __construct(ConfigRepository $configRepository)
    {
        $this->configRepository = $configRepository;
        $this->schema = $this->getSchema();
    }

    private function getSchema()
    {
        $schema = $this->configRepository->get(self::SCHEMA);
        return $schema ?: [];
    }

    /**
     * @param User $user
     * @param String $route
     * @return bool
     * @throws \Exception
     */
    public function checkPermissionGranted(User $user, String $route): bool
    {
        $roleName = $this->getRouteRoleName($route);
        if (empty($roleName)) {
            return true;
        }

        return $user->hasRole($roleName);
    }

    /**
     * @param string $route
     * @return mixed
     */
    private function getRouteRoleName(string $route)
    {
        return $this->schema[$route] ?? [];
    }

}
