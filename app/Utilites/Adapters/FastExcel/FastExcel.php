<?php

namespace App\Utilites\Adapters\FastExcel;

use Illuminate\Support\Str;

class FastExcel extends \Rap2hpoutre\FastExcel\FastExcel
{

    /**
     * @param $path
     *
     * @return string
     */
    protected function getType($path)
    {
        if (Str::endsWith($path, Type::CSV) || Str::endsWith($path, Type::PSV)) {
            return Type::CSV;
        } elseif (Str::endsWith($path, Type::ODS)) {
            return Type::ODS;
        } else {
            return Type::XLSX;
        }
    }
}