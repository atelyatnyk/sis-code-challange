<?php

namespace App\Utilites\Adapters\FastExcel;

use Box\Spout\Common\Type as FastExcelType;

class Type extends FastExcelType
{
    const PSV = 'psv';
}