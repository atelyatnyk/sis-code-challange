<?php

namespace App\Models;

use Spatie\Permission\Contracts\Role as SpatieRoleInterface;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole implements SpatieRoleInterface
{

    public const ROLE_ADMIN = 'admin';
    public const ROLE_USER = 'user';
}