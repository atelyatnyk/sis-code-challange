<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    public const COLUMN_DATE = 'date';
    public const COLUMN_CATEGORY_ID = 'category_id';
    public const COLUMN_EMPLOYEE_ID = 'employee_id';
    public const COLUMN_DESCRIPTION = 'description';
    public const COLUMN_PRE_TAX_AMOUNT = 'pre_tax_amount';
    public const COLUMN_TAX_AMOUNT = 'tax_amount';

    protected $fillable = [
        self::COLUMN_DATE,
        self::COLUMN_CATEGORY_ID,
        self::COLUMN_EMPLOYEE_ID,
        self::COLUMN_DESCRIPTION,
        self::COLUMN_PRE_TAX_AMOUNT,
        self::COLUMN_TAX_AMOUNT
    ];

    protected $dates = [
        self::COLUMN_DATE
    ];

    /**
     * @return Carbon
     */
    public function getDate(): Carbon
    {
        return $this->getAttribute(self::COLUMN_DATE);
    }

    /**
     * @param Carbon $value
     */
    public function setDate(Carbon $value): void
    {
        $this->setAttribute(self::COLUMN_DATE, $value);
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->getAttribute(self::COLUMN_CATEGORY_ID);
    }

    /**
     * @param int $value
     */
    public function setCategoryId(int $value): void
    {
        $this->setAttribute(self::COLUMN_CATEGORY_ID, $value);
    }

    /**
     * @return int
     */
    public function getEmployeeId(): int
    {
        return $this->getAttribute(self::COLUMN_EMPLOYEE_ID);
    }

    /**
     * @param int $value
     */
    public function setEmployeeId(int $value): void
    {
        $this->setAttribute(self::COLUMN_EMPLOYEE_ID, $value);
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->getAttribute(self::COLUMN_DESCRIPTION);
    }

    /**
     * @param string $value
     */
    public function setDescription(string $value): void
    {
        $this->setAttribute(self::COLUMN_DESCRIPTION, $value);
    }

    /**
     * @return float
     */
    public function getPreTaxAmount(): float
    {
        return $this->getAttribute(self::COLUMN_PRE_TAX_AMOUNT);
    }

    /**
     * @param float $value
     */
    public function setPreTaxAmount(float $value): void
    {
        $this->setAttribute(self::COLUMN_PRE_TAX_AMOUNT, $value);
    }

    /**
     * @return float
     */
    public function getTaxAmount(): float
    {
        return $this->getAttribute(self::COLUMN_TAX_AMOUNT);
    }

    /**
     * @param float $value
     */
    public function setTaxAmount(float $value): void
    {
        $this->setAttribute(self::COLUMN_TAX_AMOUNT, $value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    /**
     * @return Employee
     */
    public function getEmployee(): Employee
    {
        return $this->employee;
    }
}