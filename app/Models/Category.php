<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public const COLUMN_SLUG = 'slug';
    public const COLUMN_TITLE = 'title';

    protected $fillable = [
        self::COLUMN_SLUG,
        self::COLUMN_TITLE
    ];

    public $timestamps = false;

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->getAttribute(self::COLUMN_SLUG);
    }

    /**
     * @param string $value
     */
    public function setSlug(string $value): void
    {
        $this->setAttribute(self::COLUMN_SLUG, $value);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->getAttribute(self::COLUMN_TITLE);
    }

    /**
     * @param string $value
     */
    public function setTitle(string $value): void
    {
        $this->setAttribute(self::COLUMN_TITLE, $value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }
}