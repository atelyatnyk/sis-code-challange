<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public const COLUMN_NAME = 'name';
    public const COLUMN_ADDRESS = 'address';
    public const COLUMN_USER_ID = 'user_id';

    protected $fillable = [
        self::COLUMN_NAME,
        self::COLUMN_ADDRESS,
        self::COLUMN_USER_ID,
    ];

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getAttribute(self::COLUMN_NAME);
    }

    /**
     * @param string $value
     */
    public function setName(string $value): void
    {
        $this->setAttribute(self::COLUMN_NAME, $value);
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->getAttribute(self::COLUMN_ADDRESS);
    }

    /**
     * @param string $value
     */
    public function setAddress(string $value): void
    {
        $this->setAttribute(self::COLUMN_ADDRESS, $value);
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->getAttribute(self::COLUMN_USER_ID);
    }

    /**
     * @param int $value
     */
    public function setUserId(int $value): void
    {
        $this->setAttribute(self::COLUMN_USER_ID, $value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }

    /**
     * @return mixed
     */
    public function getExpenses(): Collection
    {
        return $this->expenses;
    }
}