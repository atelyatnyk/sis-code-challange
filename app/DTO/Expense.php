<?php

namespace App\DTO;

use App\Models\Expense as ModelExpense;
use Carbon\Carbon;

class Expense
{
    public const DATE = 'date';
    public const CATEGORY = 'category';
    public const CUSTOM_CATEGORY = 'custom_category';
    public const DESCRIPTION = 'description';
    public const PRE_TAX_AMOUNT = 'pre_tax_amount';
    public const TAX_AMOUNT = 'tax_amount';

    /**
     * @var Carbon
     */
    private $date;

    /**
     * @var string
     */
    private $category;

    /**
     * @var int
     */
    private $categoryId;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $preTaxAmount;

    /**
     * @var float
     */
    private $taxAmount;

    /**
     * @var int
     */
    private $employeeId;

    /**
     * Expense constructor.
     * @param array $data
     * @param int $employeeId
     */
    public function __construct(array $data, int $employeeId)
    {
        $this->setDate(data_get($data, self::DATE));
        $this->setCategory(
            data_get($data, self::CATEGORY)
                ? data_get($data, self::CATEGORY)
                : data_get($data, self::CUSTOM_CATEGORY)
        );
        $this->setDescription(data_get($data, self::DESCRIPTION));
        $this->setPreTaxAmount(data_get($data, self::PRE_TAX_AMOUNT));
        $this->setTaxAmount(data_get($data, self::TAX_AMOUNT));
        $this->setEmployeeId($employeeId);
    }

    /**
     * @return Carbon
     */
    public function getDate(): Carbon
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = Carbon::parse($date);
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getPreTaxAmount(): float
    {
        return $this->preTaxAmount;
    }

    /**
     * @param float $preTaxAmount
     */
    public function setPreTaxAmount(float $preTaxAmount): void
    {
        $this->preTaxAmount = $preTaxAmount;
    }

    /**
     * @return float
     */
    public function getTaxAmount(): float
    {
        return $this->taxAmount;
    }

    /**
     * @param float $taxAmount
     */
    public function setTaxAmount(float $taxAmount): void
    {
        $this->taxAmount = $taxAmount;
    }

    /**
     * @return int
     */
    public function getEmployeeId(): int
    {
        return $this->employeeId;
    }

    /**
     * @param int $employeeId
     */
    public function setEmployeeId(int $employeeId): void
    {
        $this->employeeId = $employeeId;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }


    /**
     * @return array
     */
    public function toExpenseData(): array
    {
        return [
            ModelExpense::COLUMN_DATE => $this->getDate(),
            ModelExpense::COLUMN_CATEGORY_ID => $this->getCategoryId(),
            ModelExpense::COLUMN_EMPLOYEE_ID => $this->getEmployeeId(),
            ModelExpense::COLUMN_DESCRIPTION => $this->getDescription(),
            ModelExpense::COLUMN_PRE_TAX_AMOUNT => $this->getPreTaxAmount(),
            ModelExpense::COLUMN_TAX_AMOUNT => $this->getTaxAmount(),
        ];
    }

}