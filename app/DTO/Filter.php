<?php

namespace App\DTO;

use Carbon\Carbon;

class Filter
{

    public const CATEGORY_ASC = 'category';
    public const CATEGORY_DESC = '-category';

    public const DATE_BETWEEN = 'date_between';
    public const FROM_DATE = 'from';
    public const TO_DATE = 'to';
    public const SORT_CATEGORY = 'sort_category';

    /**
     * @var Carbon|null
     */
    private $fromDate;

    /**
     * @var Carbon|null
     */
    private $toDate;

    /**
     * @var bool|null
     */
    private $categorySortAsc;

    /**
     * @return Carbon|null
     */
    public function getFromDate(): ?Carbon
    {
        return $this->fromDate;
    }

    /**
     * @param string $fromDate
     */
    public function setFromDate(?string $fromDate): void
    {
        $this->fromDate = $fromDate ? Carbon::parse($fromDate) : null;
    }

    /**
     * @return Carbon|null
     */
    public function getToDate(): ?Carbon
    {
        return $this->toDate;
    }

    /**
     * @param string $toDate
     */
    public function setToDate(?string $toDate): void
    {
        $this->toDate = $toDate ? Carbon::parse($toDate) : null;
    }

    /**
     * @return bool
     */
    public function getCategorySortAsc(): ?bool
    {
        return $this->categorySortAsc;
    }

    /**
     * @param string $categorySortAsc
     */
    public function setCategorySortAsc(?string $categorySortAsc): void
    {
        $this->categorySortAsc = $categorySortAsc;

        if ($categorySortAsc === self::CATEGORY_ASC) {
            $this->categorySortAsc = true;
        }
        if ($categorySortAsc === self::CATEGORY_DESC) {
            $this->categorySortAsc = false;
        }
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        $result = [];
        if ($this->getFromDate()) {
            $result[self::DATE_BETWEEN] = [
                self::FROM_DATE => $this->getFromDate(),
                self::TO_DATE => $this->getToDate()
            ];
        }
        if ($this->getCategorySortAsc() !== null) {
            $result[self::SORT_CATEGORY] = $this->getCategorySortAsc();
        }

        return $result;
    }

}