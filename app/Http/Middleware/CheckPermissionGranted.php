<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Src\Access\Contracts\CheckAccessService;
use Closure;
use Illuminate\Http\Request;

class CheckPermissionGranted
{
    /**
     * @var CheckAccessService
     */
    private $checkAccessService;

    /**
     * CheckPermissionGranted constructor.
     * @param CheckAccessService $checkAccessService
     */
    public function __construct(CheckAccessService $checkAccessService)
    {
        $this->checkAccessService = $checkAccessService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if (!$user) {
            return redirect()->route('login');
        }

        $path = $this->getRouteName($request);
        if ($this->isPermissionGranted($user, $path)) {
            return $next($request);
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    private function getRouteName(Request $request)
    {
        return $request->route()->getName();
    }

    /**
     * @param User $user
     * @param $path
     * @return bool
     */
    private function isPermissionGranted(User $user, $path): bool
    {
        return $this->checkAccessService->checkPermissionGranted($user, $path);
    }
}
