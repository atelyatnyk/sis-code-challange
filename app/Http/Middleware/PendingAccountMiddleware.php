<?php

namespace App\Http\Middleware;

use App\Models\Role;
use App\Models\User;
use Closure;

class PendingAccountMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var User $user */
        $user = $request->user();

        if (!$user) {
            return redirect()->route('login');
        }

        if ($user->hasRole(Role::ROLE_USER) && !$user->getEmployee()) {
            return redirect()->route('employee.assign.pending');
        }

        return $next($request);
    }

}
