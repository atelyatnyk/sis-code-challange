<?php

namespace App\Http\Requests;

use App\Http\Requests\Contracts\FilterExpensesRequest as FilterExpensesRequestInterface;
use Illuminate\Foundation\Http\FormRequest;

class FilterExpensesRequest extends FormRequest implements FilterExpensesRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filter.*' => 'nullable|date_format:m/d/Y',
            self::SORT => sprintf(
                'nullable|in:%s,%s',
                self::SORT_ASC,
                self::SORT_DESC
            ),
        ];
    }
}
