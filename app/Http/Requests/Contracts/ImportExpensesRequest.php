<?php

namespace App\Http\Requests\Contracts;

interface ImportExpensesRequest
{
    public const FILE = 'file';
}