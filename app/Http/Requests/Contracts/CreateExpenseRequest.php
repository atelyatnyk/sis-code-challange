<?php

namespace App\Http\Requests\Contracts;

interface CreateExpenseRequest
{
    public const DATE = 'date';
    public const CATEGORY = 'category';
    public const CUSTOM_CATEGORY = 'custom_category';
    public const DESCRIPTION = 'description';
    public const PER_TAX_AMOUNT = 'pre_tax_amount';
    public const TAX_AMOUNT = 'tax_amount';
}