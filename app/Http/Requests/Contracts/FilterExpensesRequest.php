<?php

namespace App\Http\Requests\Contracts;

interface FilterExpensesRequest
{
    public const FILTER_FROM_DATE = 'date_from';
    public const FILTER_TO_DATE = 'date_to';
    public const FILTER= 'filter';

    public const SORT = 'sort';
    public const SORT_ASC = 'category';
    public const SORT_DESC = '-category';
}