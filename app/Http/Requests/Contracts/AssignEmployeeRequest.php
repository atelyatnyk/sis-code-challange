<?php

namespace App\Http\Requests\Contracts;

interface AssignEmployeeRequest
{
    public const USER_ID = 'user_id';
}