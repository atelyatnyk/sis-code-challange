<?php

namespace App\Http\Requests;

use App\Http\Requests\Contracts\AssignEmployeeRequest as AssignEmployeeRequestInterface;
use Illuminate\Foundation\Http\FormRequest;

class AssignEmployeeRequest extends FormRequest implements AssignEmployeeRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::USER_ID => 'required|numeric|exists:users,id'
        ];
    }
}
