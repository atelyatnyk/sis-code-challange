<?php

namespace App\Http\Requests;

use App\Http\Requests\Contracts\ImportExpensesRequest as ImportExpensesRequestInterface;
use Illuminate\Foundation\Http\FormRequest;

class ImportExpensesRequest extends FormRequest implements ImportExpensesRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // TODO: mime types for psv
        return [
            self::FILE => 'required'
        ];
    }
}
