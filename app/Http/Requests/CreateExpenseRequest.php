<?php

namespace App\Http\Requests;

use App\Http\Requests\Contracts\CreateExpenseRequest as CreateExpenseRequestInterface;
use Illuminate\Foundation\Http\FormRequest;

class CreateExpenseRequest extends FormRequest implements CreateExpenseRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::DATE => 'required|date_format:m/d/Y',
            self::CATEGORY => 'nullable|string',
            self::CUSTOM_CATEGORY => sprintf(
                'required_if:%s,$s|string',
                self::CATEGORY,
                null
            ),
            self::DESCRIPTION => 'required|string',
            self::PER_TAX_AMOUNT => 'required|numeric',
            self::TAX_AMOUNT => 'required|numeric',
        ];
    }
}
