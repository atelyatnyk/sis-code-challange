<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImportExpensesRequest;
use App\Src\Expenses\Contracts\ImportService;

class AdminController extends Controller
{
    /**
     * @var ImportService
     */
    private $import;

    /**
     * AdminController constructor.
     * @param ImportService $import
     */
    public function __construct(ImportService $import)
    {
        $this->import = $import;
        $this->middleware('auth');
    }

    /**
     * Shows form to import file with list of expenses
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('import.form');
    }

    /**
     * Import data
     *
     * @param ImportExpensesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function import(ImportExpensesRequest $request)
    {
        $imported = $this->import->import($request->file($request::FILE));

        if ($imported) {
            return view('import.imported', ['imported' => $imported, ]);
        }

        return redirect()->back()->withErrors(['Non valid file']);
    }


}
