<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssignEmployeeRequest;
use App\Models\Employee;
use App\Src\Employee\Contracts\EmployeeManageService;
use App\Src\User\Contracts\UserManageService;

class AssignController extends Controller
{

    /**
     * @var EmployeeManageService
     */
    private $employeeManageService;

    /**
     * @var UserManageService
     */
    private $userManageService;

    /**
     * AssignController constructor.
     * @param EmployeeManageService $employeeManageService
     * @param UserManageService $userManageService
     */
    public function __construct(
        EmployeeManageService $employeeManageService,
        UserManageService $userManageService
    ) {
        $this->employeeManageService = $employeeManageService;
        $this->userManageService = $userManageService;

        $this->middleware('auth');
    }

    /**
     * Shows assign form to assign user to employee record
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $employees = $this->employeeManageService->getNotAssignedPaginated($this->perPage);
        $users = $this->userManageService->getNotAssigned();

        return view('employee.assign', [
            'employees' => $employees,
            'users' => $users,
        ]);
    }

    /**
     * Store data
     *
     * @param AssignEmployeeRequest $request
     * @param Employee $employee
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AssignEmployeeRequest $request, Employee $employee)
    {
        $result = $this->employeeManageService->assignUser($employee, $request->get($request::USER_ID));

        return $result
            ? redirect()->back()->with('success', [trans('User has been successfully assigned to the employee record.'),])
            : redirect()->back()->with('error', [trans('An error occurred.'),]);
    }

    /**
     * Shows message about pending assigment to employee record
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pending()
    {
        return view('employee.needsassign');
    }

}
