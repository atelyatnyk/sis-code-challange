<?php

namespace App\Http\Controllers;

use App\DTO\Filter;
use App\Http\Requests\FilterExpensesRequest;
use App\Src\Expenses\Contracts\ExpensesManageService;

class ExpensesController extends Controller
{
    /**
     * @var ExpensesManageService
     */
    private $expensesManageService;

    /**
     * ExpensesController constructor.
     * @param ExpensesManageService $expensesManageService
     */
    public function __construct(ExpensesManageService $expensesManageService)
    {
        $this->expensesManageService = $expensesManageService;
    }

    /**
     * Gets list of expenses
     *
     * @param FilterExpensesRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(FilterExpensesRequest $request)
    {
        $filter = new Filter();
        $filter->setFromDate(data_get($request->get($request::FILTER), $request::FILTER_FROM_DATE, null));
        $filter->setToDate(data_get($request->get($request::FILTER), $request::FILTER_TO_DATE, null));
        $filter->setCategorySortAsc($request->get($request::SORT));

        $expenses = $this->expensesManageService->getAllPaginated(
            $request->user(),
            $this->perPage,
            $filter->getFilters()
        );

        return view('expenses.list', [
            'expenses' => $expenses,
        ]);
    }

}
