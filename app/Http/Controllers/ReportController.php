<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\DTO\Expense;
use App\Http\Requests\CreateExpenseRequest;
use App\Models\User;
use App\Src\Category\Repository\Contracts\CategoryRepository;
use App\Src\Expenses\Contracts\ExpensesManageService;

class ReportController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var ExpensesManageService
     */
    private $expensesManageService;

    /**
     * ReportController constructor.
     * @param CategoryRepository $categoryRepository
     * @param ExpensesManageService $expensesManageService
     */
    public function __construct(CategoryRepository $categoryRepository, ExpensesManageService $expensesManageService)
    {
        $this->categoryRepository = $categoryRepository;
        $this->expensesManageService = $expensesManageService;
    }


    /**
     * Shows form to send report about user's expenses
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('report.form', [
            'categories' => $this->categoryRepository->getAll(),
        ]);
    }

    /**
     * Save user's expenses
     *
     * @param CreateExpenseRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(CreateExpenseRequest $request)
    {
        /**
         * @var User $user
         */
        $user = $request->user();
        $expenseData = new Expense($request->validated(), $user->getEmployee()->getKey());
        $expense = $this->expensesManageService->create($expenseData);

        return $expense
            ? redirect()->route('expenses.list')
            : redirect()->back()->with('error', [trans('An error occurred.'),]);
    }

}
