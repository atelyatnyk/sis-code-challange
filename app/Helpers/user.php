<?php

if (! function_exists('get_logged_user')) {
    /**
     * Get current user model instance.
     *
     * @return \App\Models\User|null
     */
    function get_logged_user(): ?\App\Models\User
    {
        $user = Auth::user();

        return ($user instanceof \App\Models\User) ? $user : null;
    }
}

if (! function_exists('get_logged_user_id')) {
    /**
     * Get current user model instance.
     *
     * @return int|null
     */
    function get_logged_user_id(): ?int
    {
        $user = get_logged_user();

        return !$user ? null : $user->getKey();
    }
}
