<?php

namespace App\Providers;

use App\Utilites\Adapters\Config\Contracts\ConfigRepository;
use App\Utilites\Adapters\Container\Contracts\Container;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ConfigRepository::class,
            \App\Utilites\Adapters\Config\ConfigRepository::class
        );
        $this->app->bind(
            Container::class,
            \App\Utilites\Adapters\Container\Container::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('can', function ($permission) {
            return optional(get_logged_user())->hasPermissionTo($permission);
        });

        Blade::if('hasRole', function ($role) {
            return optional(get_logged_user())->hasRole($role);
        });
    }
}
