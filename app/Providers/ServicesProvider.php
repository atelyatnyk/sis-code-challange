<?php

namespace App\Providers;

use App\Src\Access\Contracts\CheckAccessService;
use App\Src\Category\Contracts\CategoryManageService;
use App\Src\Employee\Contracts\EmployeeManageService;
use App\Src\User\Contracts\UserManageService;
use Illuminate\Support\ServiceProvider;

class ServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            CheckAccessService::class,
            \App\Src\Access\CheckAccessService::class
        );

        $this->app->bind(
            UserManageService::class,
            \App\Src\User\UserManageService::class
        );

        $this->app->bind(
            CategoryManageService::class,
            \App\Src\Category\CategoryManageService::class
        );

        $this->app->bind(
            EmployeeManageService::class,
            \App\Src\Employee\EmployeeManageService::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
