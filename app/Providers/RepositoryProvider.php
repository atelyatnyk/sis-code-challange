<?php

namespace App\Providers;

use App\Src\Category\Repository\Contracts\CategoryRepository;
use App\Src\Employee\Repository\Contracts\EmployeeRepository;
use App\Src\Expenses\Repository\Contracts\ExpensesRepository;
use App\Src\User\Repository\Contracts\UserRepository;
use App\Utilites\Repositories\Contracts\Repository;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            Repository::class,
            \App\Utilites\Repositories\Repository::class
        );

        $this->app->bind(
            ExpensesRepository::class,
            \App\Src\Expenses\Repository\ExpensesRepository::class
        );

        $this->app->bind(
            CategoryRepository::class,
            \App\Src\Category\Repository\CategoryRepository::class
        );

        $this->app->bind(
            UserRepository::class,
            \App\Src\User\Repository\UserRepository::class
        );

        $this->app->bind(
            EmployeeRepository::class,
            \App\Src\Employee\Repository\EmployeeRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
