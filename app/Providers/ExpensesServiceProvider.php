<?php

namespace App\Providers;

use App\Src\Expenses\Contracts\ExpensesManageService;
use App\Src\Expenses\Contracts\ImportService;
use App\Src\Expenses\Import\Contracts\DestroyFile;
use App\Src\Expenses\Import\Contracts\ImportFile;
use App\Src\Expenses\Import\Contracts\LoadFile;
use Illuminate\Support\ServiceProvider;

class ExpensesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ImportService::class,
            \App\Src\Expenses\ImportService::class
        );

        $this->app->bind(
            LoadFile::class,
            \App\Src\Expenses\Import\LoadFile::class
        );

        $this->app->bind(
            DestroyFile::class,
            \App\Src\Expenses\Import\DestroyFile::class
        );

        $this->app->bind(
            ImportFile::class,
            \App\Src\Expenses\Import\ImportFile::class
        );

        $this->app->bind(
            ExpensesManageService::class,
            \App\Src\Expenses\ExpensesManageService::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
