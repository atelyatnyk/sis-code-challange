<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @var User $admin
         */
        $admin = User::firstOrCreate(
            [User::COLUMN_EMAIL => 'aaa@aaa.aaa'],
            [User::COLUMN_PASSWORD => Hash::make('aaa')]
        );

        $admin->assignRole(Role::ROLE_ADMIN);
    }
}
