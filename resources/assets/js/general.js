import $ from 'jquery';
window.$ = window.jQuery = $;

import 'jquery-ui/ui/widgets/datepicker.js';

$(document).ready(function () {

    // show filepath for input file elements
    $('input[type="file"]').on('change', function () {
        var fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
    });

    // add date input
    $('[data-field-type="date"]').datepicker();
    $('[data-field-type="date_from"]').datepicker({
        changeMonth: true,
        onSelect: function (date) {
            var selectedDate = new Date(date);
            var msecsInADay = 86400000;
            var endDate = new Date(selectedDate.getTime() + msecsInADay);
            $('[data-field-type="date_to"]').datepicker('option', 'minDate', endDate);
        }
    });
    $('[data-field-type="date_to"]').datepicker({
        changeMonth: true
    });

});
