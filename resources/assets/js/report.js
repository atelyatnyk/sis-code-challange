$(document).ready(function () {

    // show/hide custom category field
    $('[id="category"]').on('change', function(event) {
        var selectedValue = $(this).val();
        var sections = $('[id="custom_category_section"]');
        $.each(sections, function(key, section) {
            var value = $(section).attr('data-showif');
            if (selectedValue == value) {
                $(section).removeClass('hide');
                var selects = $(section).find('select');
                $.each(selects, function(key, select) {
                    $(this).val($(this).find('option:first').val());
                });
                $(section).find('input, select, textarea')
                    .prop('required', true)
                    .prop('disabled', false);
            } else {
                $(section).addClass('hide');
                $(section).find('input, select, textarea')
                    .val('')
                    .prop('required', false)
                    .prop('disabled', true);
            }
        });
    });

    // init change event on page loading
    $('[id="category"]').change();

});
