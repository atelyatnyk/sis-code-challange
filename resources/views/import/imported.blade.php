@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Imported') }}</div>

                    @hasRole('admin')

                        <div class="card-body">

                            @if ($imported->isNotEmpty())

                                <div class="table-container">
                                    <table class="table table-striped text-center">
                                        <thead>
                                            <tr>
                                                <th scope="col">{{ __('Month') }}</th>
                                                <th scope="col">{{ __('Pre Tax Amount') }}</th>
                                                <th scope="col">{{ __('Tax Amount') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $total = $imported->get('total');
                                                $imported->forget('total');
                                            @endphp
                                            @foreach ($imported as $line => $record)
                                                <tr>
                                                    <td scope="row">{{ __($line) }}</td>
                                                    <td scope="row">{{ $record->get('pre_tax_amount') }}</td>
                                                    <td scope="row">{{ $record->get('tax_amount') }}</td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <th scope="row">{{ __('TOTAL') }}</th>
                                                <th scope="row">{{ $total->get('pre_tax_amount') }}</th>
                                                <th scope="row">{{ $total->get('tax_amount') }}</th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <p>{{ __('There are no records.') }}</p>
                            @endif

                        </div>

                    @endhasRole

                </div>
            </div>
        </div>
    </div>
@endsection
