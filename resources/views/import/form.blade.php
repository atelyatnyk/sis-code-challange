@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Import') }}</div>

                        @hasRole('admin')

                            <div class="card-body">
                                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.import.store') }}">
                                    @csrf

                                    <!-- File -->
                                    <div class="form-group row">
                                        <label for="file" class="col-md-4 col-form-label text-md-right">
                                            {{ __('Choose file for importing') }}*
                                        </label>

                                        <div class="col-md-6">
                                            <div class="custom-file">
                                                <input
                                                    type="file"
                                                    class="custom-file-input @error('file') is-invalid @enderror"
                                                    id="file"
                                                    name="file"
                                                    accept=".psv" />
                                                <label class="custom-file-label" for="file">
                                                    {{ __('Choose file for importing') }}
                                                </label>
                                            </div>

                                            @error('file')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Import') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        @endhasRole

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
