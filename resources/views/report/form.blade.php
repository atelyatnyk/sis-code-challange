@extends('layouts.app')

@section('pagespecificscripts')
    <script src="{{ asset('/js/report.js') }}" defer></script>
@stop

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Report') }}</div>

                    @hasRole('user')

                        <div class="card-body">
                            <form method="POST" action="{{ route('user.report.store') }}">
                                @csrf

                                <!-- Date -->
                                <div class="form-group row">
                                    <label for="date" class="col-md-4 col-form-label text-md-right">
                                        {{ __('Date') }}*
                                    </label>
                                    <div class="col-md-6">
                                        <div class="form-group input-group">
                                            <input type="text" class="form-control @error('date') is-invalid @enderror"
                                                id="datepicker" name="date" data-field-type="date" autocomplete="off" required
                                                value="{{ old('date') }}" placeholder="{{ __('Date') }}" />
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                        @error('date')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <!-- Category -->
                                <div class="form-group row">
                                    <label for="category" class="col-md-4 col-form-label text-md-right">
                                        {{ __('Category') }}*
                                    </label>
                                    <div class="col-md-6">
                                        <div class="form-group input-group">
                                            <select class="custom-select @error('category') is-invalid @enderror"
                                                id="category" name="category">
                                                @foreach ($categories as $category)
                                                    <option value="{{ $category->getTitle() }}"
                                                        {{ $category->getTitle() === old('category') ? 'selected' : '' }}>
                                                        {{ __($category->getTitle()) }}
                                                    </option>
                                                @endforeach
                                                <option value="">
                                                    {{ __('Custom category') }}
                                                </option>
                                            </select>
                                        </div>
                                        @error('category')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div id="custom_category_section" class="hide" data-showif="">
                                    <!-- Custom Category -->
                                    <div class="form-group row">
                                        <label for="custom_category" class="col-md-4 col-form-label text-md-right">
                                            {{ __('Custom category') }}*
                                        </label>
                                        <div class="col-md-6">
                                            <div class="form-group input-group">
                                                <input type="text" class="form-control @error('custom_category') is-invalid @enderror"
                                                    id="custom_category" name="custom_category" minlength="1" maxlength="255"
                                                    value="{{ old('custom_category') }}" placeholder="{{ __('Custom category') }}" />
                                            </div>
                                            @error('custom_category')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <!-- Description -->
                                <div class="form-group row">
                                    <label for="description" class="col-md-4 col-form-label text-md-right">
                                        {{ __('Description') }}*
                                    </label>
                                    <div class="col-md-6">
                                        <div class="form-group input-group">
                                            <textarea class="form-control @error('description') is-invalid @enderror"
                                                id="description" name="description"
                                                minlength="1" maxlength="255" size="255" required
                                                placeholder="{{ __('Description') }}"
                                            >{{ old('description') }}</textarea>
                                        </div>
                                        @error('description')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <!-- Pre Tax Amount -->
                                <div class="form-group row">
                                    <label for="pre_tax_amount" class="col-md-4 col-form-label text-md-right">
                                        {{ __('Pre Tax Amount') }}*
                                    </label>
                                    <div class="col-md-6">
                                        <div class="form-group input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <i class="fa fa-dollar"></i>
                                                </span>
                                            </div>
                                            <input type="number" class="form-control @error('pre_tax_amount') is-invalid @enderror"
                                                id="pre_tax_amount" name="pre_tax_amount" required min="0" max="999999.99" step="0.01"
                                                value="{{ old('pre_tax_amount') }}" placeholder="{{ __('Pre Tax Amount') }}" />
                                        </div>
                                        @error('pre_tax_amount')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <!-- Tax Amount -->
                                <div class="form-group row">
                                    <label for="tax_amount" class="col-md-4 col-form-label text-md-right">
                                        {{ __('Tax Amount') }}*
                                    </label>
                                    <div class="col-md-6">
                                        <div class="form-group input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <i class="fa fa-dollar"></i>
                                                </span>
                                            </div>
                                            <input type="number" class="form-control @error('tax_amount') is-invalid @enderror"
                                                id="tax_amount" name="tax_amount" required min="0" max="999999.99" step="0.01"
                                                value="{{ old('pre_tax_amount') }}" placeholder="{{ __('Tax Amount') }}" />
                                        </div>
                                        @error('tax_amount')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Report') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    @endhasRole

                </div>
            </div>
        </div>
    </div>
@endsection
