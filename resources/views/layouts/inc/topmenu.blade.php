<button class="navbar-toggler" type="button"
    data-toggle="collapse" data-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent" aria-expanded="false"
    aria-label="{{ __('Toggle navigation') }}">

    <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">

    <!-- Left Side Of Navbar -->
    <ul class="navbar-nav">
        @auth
            <li class="nav-item {{ \Request::route()->getName() == 'expenses.list' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('expenses.list') }}">
                    {{ __('Expenses') }}
                </a>
            </li>
        @endauth

        @hasRole('admin')
            <li class="nav-item {{ \Request::route()->getName() == 'admin.import' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.import') }}">
                    {{ __('Import') }}
                </a>
            </li>
            <li class="nav-item {{ \Request::route()->getName() == 'employee.assign' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('employee.assign') }}">
                    {{ __('Manage employees') }}
                </a>
            </li>
        @endhasRole

        @hasRole('user')
            <li class="nav-item {{ \Request::route()->getName() == 'user.report' ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('user.report') }}">
                    {{ __('Report') }}
                </a>
            </li>
        @endhasRole

    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->getEmail() }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>

</div>
