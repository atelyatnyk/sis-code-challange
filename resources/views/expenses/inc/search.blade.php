<form method="GET" action="{{ route('expenses.list') }}">
    <div class="row">

        <!-- Filter by date -->
        <div class="col-md-6 mt-1">
            <div class="row">
                <div class="form-group input-group col-md-6">
                    <input type="text" class="form-control @error('filter[date_from]') is-invalid @enderror"
                        id="datepicker_from" name="filter[date_from]" data-field-type="date_from" autocomplete="off"
                        value="{{ data_get(\Request::get('filter'), 'date_from') }}" placeholder="{{ __('Date from') }}" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group input-group col-md-6">
                    <input type="text" class="form-control @error('filter[date_to]') is-invalid @enderror"
                        id="datepicker-to" name="filter[date_to]" data-field-type="date_to" autocomplete="off"
                        value="{{ data_get(\Request::get('filter'), 'date_to')  }}" placeholder="{{ __('Date to') }}" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <!-- Sort by category -->
        <div class="col-md-4 mt-1">
            <select class="custom-select" id="sort" name="sort">
                <option value="" {{ empty(\Request::get('sort')) ? 'selected' : '' }}>
                    {{ __('No sorting') }}
                </option>
                <option value="-category" {{ \Request::get('sort') === '-category' ? 'selected' : '' }}>
                    {{ __('Sort by "Category DESC"') }}
                </option>
                <option value="category" {{ \Request::get('sort') === 'category' ? 'selected' : '' }}>
                    {{ __('Sort by "Category ASC"') }}
                </option>
            </select>
        </div>

        <div class="col-md-2 mt-1">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-search"></i>
                {{ __('Search') }}
            </button>
        </div>

    </div>
</form>
