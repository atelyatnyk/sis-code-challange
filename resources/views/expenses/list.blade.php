@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Expenses') }}</div>

                    @auth

                        <div class="card-body">

                            @if ($expenses)

                                @include('expenses.inc.search')

                                <div class="table-container">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">{{ __('Date') }}</th>
                                                <th scope="col">{{ __('Category') }}</th>
                                                <th scope="col">{{ __('Employeer\'s name') }}</th>
                                                <th scope="col">{{ __('Employeer\'s address') }}</th>
                                                <th scope="col">{{ __('Description') }}</th>
                                                <th scope="col">{{ __('Pre Tax Amount') }}</th>
                                                <th scope="col">{{ __('Tax Amount') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($expenses as $expense)
                                                <tr>
                                                    <td scope="row">{{ $expense->getKey() }}</td>
                                                    <td scope="row">{{ $expense->getDate()->format('n/j/Y') }}</td>
                                                    <td scope="row">{{ optional($expense->getCategory())->getTitle() }}</td>
                                                    <td scope="row">{{ optional($expense->getEmployee())->getName() }}</td>
                                                    <td scope="row">{{ optional($expense->getEmployee())->getAddress() }}</td>
                                                    <td scope="row">{{ $expense->getDescription() }}</td>
                                                    <td scope="row">{{ $expense->getPreTaxAmount() }}</td>
                                                    <td scope="row">{{ $expense->getTaxAmount() }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <div class="d-flex justify-content-center">
                                    {{ $expenses->links() }}
                                </div>
                            @else
                                <p>{{ __('There are no records.') }}</p>
                            @endif

                        </div>

                    @endauth

                </div>
            </div>
        </div>
    </div>
@endsection
