@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Pending account') }}</div>

                    @hasRole('user')

                        <div class="card-body">
                            <p>{{ __('Your account is waiting on assigment to employee record.') }}</p>
                        </div>

                    @endhasRole

                </div>
            </div>
        </div>
    </div>
@endsection
