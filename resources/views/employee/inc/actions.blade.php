<form class="form-inline inline-block" method="POST" action="{{ route('employee.store', ['employee' => $employee, ]) }}">

    @csrf

    <select class="custom-select @error('user_id') is-invalid @enderror"
        id="user_id" name="user_id" required>
        @foreach ($users as $user)
            <option value="{{ $user->getKey() }}"
                {{ $user->getKey() === old('user_id') ? 'selected' : '' }}>
                {{ $user->getEmail() }}
            </option>
        @endforeach
    </select>

    <button type="submit" class="btn btn-primary">
        <i class="fa fa-check"></i>
        {{ __('Assign') }}
    </button>
</form>
