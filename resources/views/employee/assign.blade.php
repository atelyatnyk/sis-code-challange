@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Assign email to employee') }}</div>

                    @hasRole('admin')

                        <div class="card-body">

                            @if ($employees)

                                <div class="table-container">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">{{ __('Employeer\'s name') }}</th>
                                                <th scope="col">{{ __('Employeer\'s address') }}</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($employees as $employee)
                                                <tr>
                                                    <td scope="row">{{ $employee->getName() }}</td>
                                                    <td scope="row">{{ $employee->getAddress() }}</td>
                                                    <td scope="row">
                                                        @include('employee.inc.actions')
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <div class="d-flex justify-content-center">
                                    {{ $employees->links() }}
                                </div>
                            @else
                                <p>{{ __('There are no records.') }}</p>
                            @endif

                        </div>

                    @endhasRole

                </div>
            </div>
        </div>
    </div>
@endsection
