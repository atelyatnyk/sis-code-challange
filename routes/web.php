<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['access', ], ], function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/assign/pending', 'AssignController@pending')->name('employee.assign.pending');

    Route::group(['middleware' => ['pending.account', ], ], function () {

        Route::get('/expenses', 'ExpensesController@index')->name('expenses.list');

        Route::get('/import', 'AdminController@index')->name('admin.import');
        Route::post('/import', 'AdminController@import')->name('admin.import.store');

        Route::get('/report', 'ReportController@index')->name('user.report');
        Route::post('/report', 'ReportController@store')->name('user.report.store');

        Route::get('/assign/employee', 'AssignController@index')->name('employee.assign');
        Route::post('/assign/employee/{employee}', 'AssignController@store')->name('employee.store');

    });

});
